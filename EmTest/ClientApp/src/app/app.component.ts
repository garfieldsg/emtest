import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private http: HttpClient) { }

  getIpLocalization() {
    const url = 'http://ip-api.com/csv';
    return this.http.get(url, { responseType: 'text' });
  }

  ngOnInit() {

    this.getIpLocalization()
      .subscribe(res => {
        console.info('data received');

        this.http.post('logs',
            { localizationData: res })
          .subscribe(res1 => {

          });
      });
  }
}
