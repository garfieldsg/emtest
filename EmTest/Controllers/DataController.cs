﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;

namespace EmTest.Controllers
{
    public class DataController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("logs")]
        public IActionResult LogData([FromBody]LogContract contract)
        {
            var content = new StringBuilder();
            content.AppendLine("-------------");
            content.AppendLine($"Data: {DateTime.Now}");
            content.AppendLine(contract.LocalizationData);
            content.AppendLine("");
            System.IO.File.AppendAllText("Logi.txt", content.ToString());
            return this.Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("logs")]
        public IActionResult ShowLogs()
        {
            return this.Ok(System.IO.File.ReadAllText("Logi.txt"));
        }
    }
}
